import React from 'react';
import { Provider } from 'react-redux';
import AppStore from './Store';

import WeatherContainer from './redux/containers/WeatherContainer';

export default class App extends React.Component {
  render() {
    return (
		<Provider store = { AppStore } >
			<WeatherContainer />
		</Provider>
    );
  }
}
