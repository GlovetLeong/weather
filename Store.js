import { createStore, combineReducers, applyMiddleware } from 'redux';
import WeatherReducer from './redux/reducers/WeatherReducer';
import thunk from 'redux-thunk';

const reducer = combineReducers({
    WeatherReducer
});

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export default store;
