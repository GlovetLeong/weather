import React from 'react';
import { View, Text, FlatList } from 'react-native';
import style from '../constants/MainStyle';

export class DateCard extends React.Component {
  render() {
    return (
      <FlatList
        data={this.props.daily}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) =>
          <View>
            <View style={ style.date_card_box }>
              <View style={ style.content }>
                <Text style={ style.date_card_time }>{ item.date }</Text>
                <Text>{ item.humidity }</Text>
                <Text style={ style.card_weather }>{ item.weather[0].main }</Text>
              </View>
              <View style={ style.expand }>
                <Text style={ style.expend_icon }>></Text>
              </View>
            </View>
          </View>
        }
      />
    );
  }
}
