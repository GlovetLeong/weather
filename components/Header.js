import React from 'react';
import { View, Text } from 'react-native';
import style from '../constants/MainStyle';

export class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
    	<View>
	    	<View style={ style.header_style }>
	    		<Text style={ style.header_text }>
	            	{ this.props.today[0].name }
	            </Text>
	    	</View>
      </View>
    );
  }
}
