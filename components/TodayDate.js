import React from 'react';
import { View, Text } from 'react-native';
import style from '../constants/MainStyle';

export class TodayDate extends React.Component {
  render() {
    return (
      <View style={{paddingVertical: 20}}>
        <Text style={ style.date_time }>
          { this.props.today[0].date }
        </Text>
        <Text style={ style.humidity }>
          { this.props.today[0].main.humidity }
        </Text>
        <Text style={ style.weather }>
          { this.props.today[0].weather[0].main }
        </Text>
      </View>
    );
  }
}
