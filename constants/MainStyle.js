import { StyleSheet } from 'react-native';
// import Layout from './Layout'

const MainStyle = {
	loading: {
		justifyContent: 'center',
		height: '100%'
	},
	//header
	header_style: {
		width: '100%', 
		height: 80, 
		justifyContent: 'center',
		backgroundColor: '#ec4334'
	},
	header_text: {
		color: '#fff',
		fontSize: 20,
		textAlign: 'center'
	},

	//selected-date
	date_time: {
		fontWeight: 'bold',
		fontSize: 20,
		textAlign: 'center'
	},
	humidity: {
		fontWeight: 'bold',
		fontSize: 40,
		textAlign: 'center'
	},
	weather: {
		color: '#9a9a9a',
		fontSize: 30,
		textAlign: 'center'
	},

	//date-card
	date_card_box: {
		flexDirection: 'row',
		width: '100%', 
		padding: 10,
		borderBottomWidth: 1,
		borderColor: '#eaeaea'
	},
	content: {
		width: '90%',
	},
	expand: {
		width: '10%',
		justifyContent: 'center',
	},
	expend_icon: {
		color: '#ec4334',
		fontWeight: 'bold',
		fontSize: 20
	},
	date_card_time: {
		fontWeight: 'bold',
	},
	card_weather: {
		color: '#9a9a9a'
	}
};

export default MainStyle;