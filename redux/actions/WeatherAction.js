import OpenWeather  from '../../constants/OpenWeather';
import { getLocation, timeConverter } from '../../utilities/Helper';
const Axios = require('axios');

export const requestWeather = () => {
  return function (dispatch) {
    getLocation().then(function(location){
      let params = {
        long: location.coords.longitude,
        lat: location.coords.latitude,
      };
      dispatch(requestTodayWeatherGet(params));
      dispatch(requestDailyWeatherGet(params));
    });
  }
}

export const requestTodayWeather = () => {
  return {
    type: 'REQUEST_TODAY_WEATHER',
    today_weather_loading: true,
  }
}

export const requestTodayWeatherGet = (params) => {
  return function (dispatch) {
    dispatch(requestTodayWeather());
    const request = Axios({
      method: 'Get',
      url: OpenWeather.api_url + 'find?lat=' + params.lat + '&lon=' + params.long + '&cnt=10&appid=' + OpenWeather.app_id,
    });

    return request.then(function(response){
      let list = response.data.list;

      list.map(function(weather, index){
        weather.date = new Date(weather.dt * 1000).toString();
      });

      dispatch(requestTodayWeatherSuccess(response.data));
    })
    .catch(function (error) {
      JSON.stringify(error);
    });
  }
}

export const requestTodayWeatherSuccess = (data) => {
  return {
    type: 'REQUEST_TODAY_WEATHER_SUCCESS',
    today_weather_loading: false,
    today_weather_data: data
  }
}

export const requestTodayWeatherError = () => {
  return {
    type: 'REQUEST_TODAY_WEATHER_ERROR',
    today_weather_loading: true,
    today_weather_data: []
  }
}

/////////////////////////////////////////////////////////////////////////////////////

export const requestDailyWeather = () => {
  return {
      type: 'REQUEST_DAILY_WEATHER',
      daily_weather_loading: true,
  }
}

export const requestDailyWeatherGet = (params) => {
  return function (dispatch) {
    dispatch(requestDailyWeather());
    const request = Axios({
      method: 'Get',
      url: OpenWeather.api_url + 'forecast/daily?lat=' + params.lat + '&lon=' + params.long + '&cnt=10&appid=' + OpenWeather.app_id,
    });

    return request.then(function(response){

      let list = response.data.list;

      list.map(function(weather, index){
        weather.date = timeConverter(weather.dt);
      });

      //reverse 
      response.data.list = Object.assign([], response.data.list).reverse();

      dispatch(requestDailyWeatherSuccess(response.data));
    });
  }
}

export const requestDailyWeatherSuccess = (data) => {
  return {
    type: 'REQUEST_DAILY_WEATHER_SUCCESS',
    daily_weather_loading: false,
    daily_weather_data: data
  }
}

export const requestDailyWeatherError = () => {
  return {
    type: 'REQUEST_DAILY_WEATHER_ERROR',
    daily_weather_loading: true,
    daily_weather_data: []
  }
}