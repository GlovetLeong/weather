import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as WeatherAction from '../actions/WeatherAction';
import HomeScreen from '../../screens/HomeScreen';

function mapStateToProps(state) {
    return {
        today_weather_loading: state.WeatherReducer.today_weather_loading,
        today_weather_data: state.WeatherReducer.today_weather_data,
        daily_weather_loading: state.WeatherReducer.daily_weather_loading,
        daily_weather_data: state.WeatherReducer.daily_weather_data,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(WeatherAction, dispatch);
}

const WrapperContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen);

export default WrapperContainer;
