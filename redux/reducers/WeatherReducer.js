const initState = {
    today_weather_loading: true,
    today_weather_data: [],
    daily_weather_loading: true,
    daily_weather_data: []
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_TODAY_WEATHER':
            return {
                ...state,
                today_weather_loading: action.today_weather_loading
            };
        case 'REQUEST_TODAY_WEATHER_SUCCESS':
            return {
                ...state,
                today_weather_loading: action.today_weather_loading,
                today_weather_data: action.today_weather_data,
            };
         case 'REQUEST_DAILY_WEATHER':
            return {
                ...state,
                daily_weather_loading: action.daily_weather_loading
            };
        case 'REQUEST_DAILY_WEATHER_SUCCESS':
            return {
                ...state,
                daily_weather_loading: action.daily_weather_loading,
                daily_weather_data: action.daily_weather_data,
            };
        default:
            return state;
    }
}