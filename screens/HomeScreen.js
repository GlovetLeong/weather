import React from 'react';
import { View, ScrollView, ActivityIndicator } from 'react-native';

import { Header } from '../components/Header';
import { TodayDate } from '../components/TodayDate';
import { DateCard } from '../components/DateCard';
import style from '../constants/MainStyle';

var lodash = require('lodash');

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.props.requestWeather();
  }

  render() {
    const today_weather_loading = this.props.today_weather_loading;
    const today = lodash.get(this.props.today_weather_data, 'list', []);

    const daily_weather_loading = this.props.daily_weather_loading;
    const daily = lodash.get(this.props.daily_weather_data, 'list', []);

    return (
    	<ScrollView>
        {
          (today_weather_loading) ?
            <View style={style.loading}>
              <ActivityIndicator size="large" color="#ec4334" />
            </View>
          :
            <View>
              <Header today={today}/>
              <TodayDate today={today}/>
              <DateCard daily={daily}/>
            </View>
        }
      </ScrollView>
    );
  }
}


export default HomeScreen;
