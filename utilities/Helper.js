export const getLocation = () => {
    return new Promise((res, rej) => {
        navigator.geolocation.getCurrentPosition(res, rej);
    });
}

export const timeConverter = (UNIX_timestamp) => {
	let a = new Date(UNIX_timestamp * 1000);
	let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
	let year = a.getFullYear();
	let month = months[a.getMonth()];
	let date = a.getDate();
	let day = days[a.getDay()];
	let hour = a.getHours();
	let min = a.getMinutes();
	let sec = a.getSeconds();
	let time = date + ' ' + month + ' ' + year + ', ' + day;
	return time;
}

